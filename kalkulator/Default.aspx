﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="kalkulator._Default" %>




<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <!-- calc -->
    <style>
        .mybody {
            color: white;
            font-family: Segoe UI;
            margin: 13px auto;
            box-shadow: #333333 1px 1px 3px 1px;
            border-collapse: separate;
            background-image: linear-gradient(to right, #3b3b3c, #5f6164);
            padding: 10px;
        }

            .mybody .tv-container {
                border: 4px solid #27aae1;
            }

            .mybody .net-container {
                border: 4px solid #f7941d;
            }

            .mybody .fiks-container {
                border: 4px solid #ec008c;
            }

            .mybody .mob-container {
                border: 4px solid #00a651;
            }

        .input-container {
            align-self: flex-start;
        }

        .paket-opis-container {
            width: 100%;
            align-self: flex-end;
            margin: 0px 10px;
        }

        .mybody h1, h2, h3, p, label {
            font-family: Segoe UI;
        }

        .mybody .headings {
            line-height: inherit;
            margin-bottom: 0px;
            font-weight: bold;
            font-size: 30px;
        }


        .mybody .heading-tv {
            color: #27aae1;
            margin-top: 5px;
        }

        .mybody .heading-internet {
            color: #f59520;
            margin-top: -11px;
        }

        .mybody .headingFiksna {
            color: #ec0089;
            margin-top: -36px;
        }

        .mybody .heading-mobilna {
            color: #00a651;
            margin-top: -16px;
        }

        .mybody span {
            font-family: Segoe UI Black;
        }

        .mybody h4 {
            font-weight: bold;
            font-size: 11px;
            font-family: Segoe UI Black;
        }

        .mybody .paket-header {
            margin-bottom: 5px;
            margin-top: 0px;
            font-weight: 700;
            margin-top: 20px;
            font-size: 15px;
            display: inline-block;
        }

        .mybody .row {
            margin: 0;
            margin-bottom: 10px;
            padding: 5px 5px 5px 10px;
        }

        .prijaviSeBtn {
            color: white;
            color: white;
            background-color: #ed2024;
            border: none;
            width: 330px;
            padding: 20px;
            font-size: 20px;
            font-weight: bold;
        }

            .prijaviSeBtn:hover {
                background-color: rgba(237, 32, 36, 0.7);
            }

        .mybody .col-md-4 {
            padding: 0;
        }

        .flex-display {
            display: flex;
            justify-content: space-between;
        }

        .mybody div label input {
            margin-right: 100px;
        }

        .mybody label {
            margin-bottom: 0;
        }

        .priceSide {
            width: 35%;
            margin-top: 25px;
        }

        .ck-button {
            border-image: linear-gradient(to right, #d7d7d7 0%, #6a6a6a 50%) !important;
            border-image-slice: 1 !important;
            border-width: 3px !important;
            border: 3px solid #b2b1b1;
            border-radius: 4px;
            margin: 4px;
            opacity: 0.2;
        }

            .ck-button label {
                float: left;
                width: 5em;
                height: 5em;
            }

                .ck-button label:hover {
                    cursor: pointer;
                }

                .ck-button label input {
                    position: absolute;
                    margin: 4px;
                    background-image: linear-gradient(to bottom, #f6ac1b, #ea8220);
                    overflow: auto;
                    float: left;
                }

        /*Samo za superTv1*/
        .ck-button-disabled {
            background-image: linear-gradient(to bottom, #1eade7, #1db6dd);
            overflow: auto;
            float: left;
            opacity: 1;
            cursor: not-allowed;
        }

            .ck-button-disabled label {
                cursor: not-allowed !important;
            }

        /*tv dugmad*/
        .ck-button-tv {
            background-image: linear-gradient(to bottom, #1eade7, #1db6dd);
            overflow: auto;
            float: left;
        }

            .ck-button-tv:hover {
                background-image: linear-gradient(to bottom, #1eade7, #1db6dd);
                cursor: pointer;
            }



        /*int dugmad*/
        .ck-button-int {
            background-image: linear-gradient(to bottom, #f6ac1b, #ea8220);
            overflow: auto;
            float: left;
        }

            .ck-button-int:hover {
                background-image: linear-gradient(to bottom, #f6ac1b, #ea8220);
                cursor: pointer;
            }

        /*fiksna dugmad*/
        .ck-button-fix {
            background-image: linear-gradient(to right, #ab4695, #da54e6);
            overflow: auto;
            float: left;
        }

            .ck-button-fix:hover {
                background-image: linear-gradient(to right, #ab4695, #da54e6);
                cursor: pointer;
            }

        /*mobilna dugmad*/
        .ck-button-mobil {
            background-image: linear-gradient(to right, #5bba78, #3fe880);
            overflow: auto;
            float: left;
        }

            .ck-button-mobil:hover {
                background-image: linear-gradient(to right, #5bba78, #3fe880);
                cursor: pointer;
            }

        .oppacityDecrese {
            opacity: 1;
        }

        .dodatna-margina-poravnanje {
            margin-top: 15px;
        }

        .shadow-cijena {
            margin: 0px;
            text-shadow: black 2px 2px 8px;
            font-weight: bold;
        }

        .vas-paket {
            border: none;
        }

        .vas-paket-cijena {
            border-top: 3px solid white;
        }

        .hide-on-mobile {
            display: block;
        }

        .show-on-mobile {
            display: none;
        }


        @media only screen and (max-width: 790px) {
            .prijaviSeBtn {
                width: 100%;
            }

            .vas-paket {
                border-top: 3px solid white;
            }

            .vas-paket-cijena {
                border-top: none;
            }

            .headingFiksna {
                margin-top: -15px !important;
            }

            .hide-on-mobile {
                display: none;
            }

            .show-on-mobile {
                display: block;
            }
        }
    </style>
    <p>&nbsp;</p>
    <div class="mybody">
        <div class="row tv-container">
            <div class="row" style="margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div class="ck-button ck-button-disabled">
                                <label>
                                    <input id="tv-predef" type="checkbox" data-id="1" data-cijena="29.25" data-ime="SUPER TV" value="1" style="display: none;" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">29,25 KM</h4>
                            </div>
                            <p class="televizija-header">
                                Osnovni paket sa preko 200 SD i HD kanala
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="2" data-cijena="6.67" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">6,67 KM</h4>
                            </div>
                            <p class="televizija-header">Plus paket sa 32 kanala</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv3" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="3" data-cijena="9.36" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">9,36 KM</h4>
                            </div>
                            <p class="televizija-header">HD paket sa 12 kanala</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 0px; margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv4" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="4" data-cijena="4.10" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container  dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">4,10 KM</h4>
                            </div>
                            <p class="televizija-header">Arena sport HD sa 5 kanala</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv4" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="4" data-cijena="5.85" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">5,85 KM</h4>
                            </div>
                            <p class="televizija-header">Filmbox paket sa 10 kanala</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv5" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="5" data-cijena="7" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">7,00 KM</h4>
                            </div>
                            <p class="televizija-header">Cinestar Premiere HD paket sa 2 kanala</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 0px; margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv7" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="7" data-cijena="9.90" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">9,90 KM</h4>
                            </div>
                            <p class="televizija-header">Zadruga paket sa 4 kanala</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv8" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="8" data-cijena="3.50" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">3,50 KM</h4>
                            </div>
                            <p class="televizija-header">Minimax VoD</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv9" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="9" data-cijena="7.50" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">7,50 KM</h4>
                            </div>
                            <p class="televizija-header">HBO HD paket sa 3 kanala</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 0px; margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="superTv10" class="ck-button ck-button-tv">
                                <label>
                                    <input type="checkbox" data-id="10" data-cijena="4.00" data-ime="SUPER TV" value="1" style="display: none;" onclick="checkUncheck(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">SUPER TV</h4>
                                <h4 class="paket-header" style="float: right;">4,00 KM</h4>
                            </div>
                            <p class="televizija-header">Cinemax HD paket sa 2 kanala</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin: 0px; padding-top: 0px;">
                <div class="col-md-4 show-on-mobile">
                    <h1 class="headings heading-tv" style="margin-top: 10px;">TELEVIZIJA</h1>
                </div>
                <div class="col-md-4 hide-on-mobile">
                    <h1 class="headings heading-tv">TELEVIZIJA</h1>
                </div>
            </div>
        </div>
        <div class="row net-container">
            <div class="row" style="margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="internet1" class="ck-button ck-button-int">
                                <label>
                                    <input id="net1" type="radio" value="1" data-cijena="10.53" data-ime="INTERNET - NET 1" name="net" style="display: none;" onclick="checkRadio(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">NET 1</h4>
                                <h4 class="paket-header" style="float: right;">10,53 KM</h4>
                            </div>
                            <p class="televizija-header">Do 50 Mbps / 3 Mbps</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="internet2" class="ck-button ck-button-int">
                                <label>
                                    <input type="radio" value="2" data-cijena="19.89" data-ime="INTERNET - NET 2" name="net" style="display: none;" onclick="checkRadio(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">NET 2</h4>
                                <h4 class="paket-header" style="float: right;">19,89 KM</h4>
                            </div>
                            <p class="televizija-header">Do 80 Mbps / 6 Mbps</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="internet3" class="ck-button ck-button-int">
                                <label>
                                    <input type="radio" value="3" data-cijena="29.25" data-ime="INTERNET - NET 3" name="net" style="display: none;" onclick="checkRadio(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">NET 3</h4>
                                <h4 class="paket-header" style="float: right;">29,25 KM</h4>
                            </div>
                            <p class="televizija-header">Do 120 Mbps / 10 Mbps</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
                <h1 class="headings heading-internet">INTERNET</h1>
            </div>
        </div>
        <div class="row fiks-container">
            <div class="row" style="margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="fiks1" class="ck-button ck-button-fix">
                                <label>
                                    <input type="checkbox" value="1" data-cijena="4.68" data-ime="FIKSNA 1" name="fiks" style="display: none;" onclick="checkFiksna(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">FIKSNA 1</h4>
                                <h4 class="paket-header" style="float: right;">4,68 KM</h4>
                            </div>
                            <p class="televizija-header">
                                <span>Besplatni pozivi</span> unutar mreže
                                <br />
                                <span>1000 besplatnih minuta</span> prema svim ﬁksnim mrežama u BiH
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="fiks2" class="ck-button ck-button-fix">
                                <label>
                                    <input type="checkbox" value="1" data-cijena="11.7" data-ime="FIKSNA 2" name="fiks" style="display: none;" onclick="checkFiksna(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">FIKSNA 2</h4>
                                <h4 class="paket-header" style="float: right;">11,70 KM</h4>
                            </div>
                            <p class="televizija-header">
                                <span>Besplatni pozivi</span> unutar mreže
                                <br />
                                <span>1500 besplatnih minuta</span> prema svim ﬁksnim mrežama u BiH
                                <br />
                                <span>50 besplatnih minuta</span> prema svim mobilnim mrežama u BiH
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="fiks3" class="ck-button ck-button-fix">
                                <label>
                                    <input type="checkbox" value="1" data-cijena="17.55" data-ime="FIKSNA 3" name="fiks" style="display: none;" onclick="checkFiksna(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">FIKSNA 3</h4>
                                <h4 class="paket-header" style="float: right;">17,55 KM</h4>
                            </div>
                            <p class="televizija-header">
                                <span>Besplatni pozivi</span> unutar mreže
                                <br />
                                <span>2000 besplatnih minuta</span> prema svim ﬁksnim mrežama u BiH
                                <br />
                                <span>100 besplatnih minuta</span> prema svim mobilnim mrežama u BiH
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
                <h1 class="headings headingFiksna">FIKSNA TELEFONIJA</h1>
            </div>
        </div>
        <div class="row mob-container">
            <div class="row" style="margin-bottom: 0px;">
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="mob1" class="ck-button ck-button-mobil">
                                <label>
                                    <input type="checkbox" value="1" data-cijena="2.34" data-ime="MOBILNA 1" name="mob" style="display: none;" onclick="checkMobilna(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">MOBILNA 1</h4>
                                <h4 class="paket-header" style="float: right;">2,34 KM</h4>
                            </div>
                            <p class="televizija-header">
                                <span>50 besplatnih minuta</span> prema svim mobilnim mrežama u BiH
                                <br />
                                <span>100 besplatnih SMS</span> poruka prema svim mobilnim mrežama u BiH
                                <br />
                                <span>1 GB</span> mobilnog interneta po maksimalnoj brzini prenosa podataka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="mob2" class="ck-button ck-button-mobil">
                                <label>
                                    <input type="checkbox" value="1" data-cijena="3.51" data-ime="MOBILNA 2" name="mob" style="display: none;" onclick="checkMobilna(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">MOBILNA 2</h4>
                                <h4 class="paket-header" style="float: right;">3,51 KM</h4>
                            </div>
                            <p class="televizija-header">
                                <span>100 besplatnih minuta</span> prema svim mobilnim mrežama u BiH
                                <br />
                                <span>100 besplatnih SMS</span> poruka prema svim mobilnim mrežama u BiH
                                <br />
                                <span>1 GB</span> mobilnog interneta po maksimalnoj brzini prenosa podataka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="flex-display">
                        <div class="input-container">
                            <div id="mob3" class="ck-button ck-button-mobil">
                                <label>
                                    <input type="checkbox" value="1" data-cijena="4.68" data-ime="MOBILNA 3" name="mob" style="display: none;" onclick="checkMobilna(this);" />
                                </label>
                            </div>
                        </div>
                        <div class="paket-opis-container dodatna-margina-poravnanje">
                            <div>
                                <h4 class="paket-header">MOBILNA 3</h4>
                                <h4 class="paket-header" style="float: right;">4,68 KM</h4>
                            </div>
                            <p class="televizija-header">
                                <span>150 besplatnih minuta</span> prema svim mobilnim mrežama u BiH
                                <br />
                                <span>250 besplatnih SMS</span> poruka prema svim mobilnim mrežama u BiH
                                <br />
                                <span>2 GB</span> mobilnog interneta po maksimalnoj brzini prenosa podataka
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
                <h1 class="headings heading-mobilna">MOBILNA TELEFONIJA</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7" style="padding: 0px; font-size: 5px;">
                <p style="font-size: 8px; margin-bottom: 2px;">
                    Sve cijene su sa PDV-om 17 %.
                </p>
                <p style="font-size: 8px; margin-bottom: 2px;">
                    Uslov za kombinaciju je minimalno Osnovni TV paket i jedan od paketa interneta. Kalkulator unaprijed
uzima u zbir paket interneta-Net 1 i Osnovni paket SUPER TV usluge.&nbsp;<br />
                    Osnovni paket uključuje sve funkcionalnosti SUPER TV usluge uključujući i SUPER WEB TV.
                </p>
                <p style="font-size: 8px; margin-bottom: 2px;">
                    Uz ugovorno vezivanje na 12 mjeseci, iznos mjesečne pretplate se umanjuje 5%.
                </p>
                <p style="font-size: 8px; margin-bottom: 20px;">
                    Uz ugovorno vezivanje na 24 mjeseca, iznos mjesečne pretplate se umanjuje 10%.
                </p>
            </div>
            <div class="col-md-5" style="padding-right: 0px;">
                <div class="row vas-paket" style="margin: 0px; padding: 0px;">
                    <div class="col-md-7" style="text-align: right; padding-left: 0px;">
                        <h4>Va&scaron; paket iznosi: </h4>
                    </div>
                    <div class="col-md-5 vas-paket-cijena" style="text-align: right; padding: 0px;">
                        <h2 id="iznos" class="shadow-cijena">106.25 KM</h2>
                    </div>
                </div>
                <div class="row" style="margin: 0px; padding: 0px;">
                    <div class="col-md-7" style="text-align: right; padding-left: 0px;">
                        <h4>-10% uz ugovor na 24 mjeseca: </h4>
                    </div>
                    <div class="col-md-5" style="text-align: right; padding: 0px;">
                        <h2 id="desetPosto" class="shadow-cijena" style="color: red;">95.63 KM</h2>
                    </div>
                </div>
                <div class="row" style="margin: 0px; padding: 0px;">
                    <div class="col-md-7" style="text-align: right; padding-left: 0px;">
                        <h4>-5% uz ugovor na 12 mjeseci: </h4>
                    </div>
                    <div class="col-md-5" style="text-align: right; border-bottom: 3px solid white; padding: 0px;">
                        <h2 id="petPosto" class="shadow-cijena">100.94 KM</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="text-align: right;">
            <button class="prijaviSeBtn" onclick="prikaziSlagalicu();">PRIJAVI SE</button>
        </div>
    </div>
    <script src="slagalica.js"></script>
    <!-- calc  -->

    <input type="hidden" id="LS_izabrano" value="" />

    <script>

        var selektovanaTelevizija = [];
        var selektovaniInternet = {};
        var selektovanaFiksna = {};
        var selektovanaMobilna = {};
        var izabrano;

        //automatski dodati supertv 1
        document.getElementById("tv-predef").click();
        //selektovanaTelevizija.push({ id: "1", cijena: "29.25", ime: "SUPER TV" });
        //trigeruj klik na radio button internet 1
        document.getElementById("net1").click();
        //$("#net1").trigger("click");
        //kalkulisi odma sumu 
        calculateSum();

        //funkcija za checkiranje televizije
        function checkUncheck(e) {

            let elementVal = e.checked;
            let parentContainer = e.parentElement.parentElement;

            let selectedElement = {
                cijena: e.dataset.cijena,
                ime: e.dataset.ime,
                id: e.dataset.id
            };

            if (elementVal) {
                parentContainer.classList.add("oppacityDecrese");
                selektovanaTelevizija.push(selectedElement);
            }
            else {
                parentContainer.classList.remove("oppacityDecrese");
                let indexOfElement = selektovanaTelevizija.findIndex(x => x.id == selectedElement.id);
                selektovanaTelevizija.splice(indexOfElement, 1);
            }

            calculateSum();
        }

        //funkcija za checkiranje interneta (barem jedna mora biti odabran)
        function checkRadio(e) {

            selektovaniInternet = {
                cijena: e.dataset.cijena,
                ime: e.dataset.ime
            };

            let netRadioButtons = document.getElementsByName("net").forEach(function (item, index) {

                let parentContainer = item.parentElement.parentElement;

                if (parentContainer.classList.contains("oppacityDecrese"))
                    parentContainer.classList.remove("oppacityDecrese");
            });

            e.parentElement.parentElement.classList.add("oppacityDecrese");

            calculateSum();
        }

        //funkcija za checkiranje fiksne
        function checkFiksna(e) {
            let parentContainer = e.parentElement.parentElement;

            selektovanaFiksna = {
                cijena: e.dataset.cijena,
                ime: e.dataset.ime
            };

            //ocisti ostale ostavi samo selektovani
            document.getElementsByName("fiks").forEach(function (item, index) {

                let parentContainer = item.parentElement.parentElement;

                if (parentContainer.classList.contains("oppacityDecrese")) {
                    item.checked = false;
                    parentContainer.classList.remove("oppacityDecrese");
                }
            });

            if (e.checked) {
                parentContainer.classList.add("oppacityDecrese");
                e.checked = true;
            } else {
                parentContainer.classList.remove("oppacityDecrese");
                e.checked = false;
                selektovanaFiksna = {};
            }

            calculateSum();
        }

        //funkcija za checkiranje mobilne
        function checkMobilna(e) {
            let parentContainer = e.parentElement.parentElement;

            selektovanaMobilna = {
                cijena: e.dataset.cijena,
                ime: e.dataset.ime
            };

            //ocisti ostale ostavi samo selektovani
            document.getElementsByName("mob").forEach(function (item, index) {

                let parentContainer = item.parentElement.parentElement;

                if (parentContainer.classList.contains("oppacityDecrese")) {
                    item.checked = false;
                    parentContainer.classList.remove("oppacityDecrese");
                }
            });

            if (e.checked) {
                parentContainer.classList.add("oppacityDecrese");
                e.checked = true;
            } else {
                parentContainer.classList.remove("oppacityDecrese");
                e.checked = false;
                selektovanaMobilna = {};
            }

            calculateSum();
        }

        function calculateSum() {

            let suma = 0;
            izabrano = "TELEVIZIJA: \n";

            //sabrane televizije
            for (var i = 0; i < selektovanaTelevizija.length; i++) {
                suma += parseFloat(selektovanaTelevizija[i].cijena);
                izabrano += selektovanaTelevizija[i].ime + "-" + selektovanaTelevizija[i].cijena + "\n";
            }

            izabrano += "INTERNET: \n"
            //sabran internet
            suma += parseFloat(selektovaniInternet.cijena);
            izabrano += selektovaniInternet.ime + "-" + selektovaniInternet.cijena + "\n";


            izabrano += "FIKSNA: \n";
            //sabrana fiksna
            if (selektovanaFiksna != null && selektovanaFiksna != undefined && Object.entries(selektovanaFiksna).length != 0) {
                suma += parseFloat(selektovanaFiksna.cijena);
                izabrano += selektovanaFiksna.ime + "-" + selektovanaFiksna.cijena + "\n";
            }

            izabrano += "MOBILNA: \n";
            //sabrana mobilna
            if (selektovanaMobilna != null && selektovanaMobilna != undefined && Object.entries(selektovanaMobilna).length != 0) {
                suma += parseFloat(selektovanaMobilna.cijena);
                izabrano += selektovanaMobilna.ime + "-" + selektovanaMobilna.cijena + "\n";
            }

            suma = suma.toFixed(2);

            document.getElementById("iznos").innerHTML = suma + " KM";
            document.getElementById("desetPosto").innerHTML = (suma - (suma * 0.1)).toFixed(2) + " KM";
            document.getElementById("petPosto").innerHTML = (suma - (suma * 0.05)).toFixed(2) + " KM";
            document.getElementById("LS_izabrano").value = izabrano;
        }
    </script>
</asp:Content>
