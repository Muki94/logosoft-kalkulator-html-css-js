﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(kalkulator.Startup))]
namespace kalkulator
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
